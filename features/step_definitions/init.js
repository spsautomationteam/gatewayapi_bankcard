/* jshint node:true */
'use strict';

var factory = require('./factory.js');
var config = require('../../config/config.json');

var SPSUser = config.MerchantOnboard.SPSUser;
var Authorization = config.MerchantOnboard.Authorization;
var InvalidSPSUser = config.MerchantOnboard.InvalidSPSUser;
var InvalidAuthorization = config.MerchantOnboard.InvalidAuthorization;


module.exports = function() {
    // cleanup before every scenario
    this.Before(function(scenario, callback) {
        this.apickli = factory.getNewApickliInstance();
        this.apickli.storeValueInScenarioScope("SPSUser", SPSUser);
        this.apickli.storeValueInScenarioScope("Authorization", Authorization);
		this.apickli.storeValueInScenarioScope("InvalidSPSUser", InvalidSPSUser);
        this.apickli.storeValueInScenarioScope("InvalidAuthorization", InvalidAuthorization);
		
        callback();
    });
};

