var apickli = require('apickli');
var config = require('../../config/config.json');

var defaultBasePath = config.MerchantOnboard.basepath;
var defaultDomain = config.MerchantOnboard.domain;

console.log('Merchant onboard api: [' + config.MerchantOnboard.domain + ', ' + config.MerchantOnboard.basepath + ']');

var getNewApickliInstance = function(basepath, domain) {
	basepath = basepath || defaultBasePath;
	domain = domain || defaultDomain;


	return new apickli.Apickli('https', domain + basepath);
};

exports.getNewApickliInstance = getNewApickliInstance;
	
