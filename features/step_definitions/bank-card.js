/* jslint node: true */
'use strict';
var apickli = require('apickli');
var factory = require('./factory.js');
var config = require('../../config/config.json');
var Contenttype = "application/json";
var AuthorizationValue;
var SPSUserValue;

var prettyJson = require('prettyjson');


var postTransactionRequestWithReference = function( apickli,referenceval,getTemplateAction,callback ) {
		var pathSuffix =getTemplateAction;
		
		var url = apickli.domain + pathSuffix;
		//console.log("apickli.domain :"+apickli.domain)
		//console.log("pathSuffix :"+pathSuffix)
		//console.log("url :"+url)
		//Math.floor(Math.random() * (Max - min + 1)) + min;
		//var random= Math.floor(Math.random() * (1000)) + 1;
		//console.log("random :"+random)
				
		apickli.addRequestHeader('accept',"application/json");
		apickli.addRequestHeader('Connection', "Keep-Alive");
		//console.log("\n@@@@@apickli.headers.Authorization ::::"+apickli.headers.Authorization)
		//apickli.scenarioVariables.Authorization;			
		//console.log("\n@@@@@ apickli.scenarioVariables.Authorization ::::"+apickli.scenarioVariables.Authorization)
		
		//console.log("######### pathSuffix:"+pathSuffix)
			apickli.post(pathSuffix, function (err, response) {
			if ( response ) {
				//console.log("\nRESPONSE >>>>>>:\n"+ response.body);
			}			
			apickli.storeValueOfResponseBodyPathInScenarioScope('$.reference', 'Reference');						
	        // console.log("Reference : "+apickli.scenarioVariables.Reference);			
        	callback();
		});
};



var postTransactionBatchRequest = function( apickli,getTemplateAction,callback ) {
		var pathSuffix =getTemplateAction;
		
		var url = apickli.domain + pathSuffix;
		//console.log("apickli.domain :"+apickli.domain)
		//console.log("pathSuffix :"+pathSuffix)
		//console.log("url :"+url)
		//Math.floor(Math.random() * (Max - min + 1)) + min;
		//var random= Math.floor(Math.random() * (1000)) + 1;
		//console.log("random :"+random)
				
		apickli.addRequestHeader('accept',"application/json");
		apickli.addRequestHeader('Connection', "Keep-Alive");
		//console.log("\n@@@@@apickli.headers.Authorization ::::"+apickli.headers.Authorization)
		//apickli.scenarioVariables.Authorization;			
		//console.log("\n@@@@@ apickli.scenarioVariables.Authorization ::::"+apickli.scenarioVariables.Authorization)
		
		//console.log("######### pathSuffix:"+pathSuffix)
			apickli.post(pathSuffix, function (err, response) {
			if ( response ) {
				//console.log("\nRESPONSE >>>>>>:\n"+ response.body);
			}			
			//apickli.storeValueOfResponseBodyPathInScenarioScope('$.reference', 'Reference');						
	        // console.log("Reference : "+apickli.scenarioVariables.Reference);			
        	callback();
		});
};

var postTransactionRequest = function( apickli,getTemplateAction,callback ) {
		var pathSuffix =getTemplateAction;
		
		var url = apickli.domain + pathSuffix;
		//console.log("apickli.domain :"+apickli.domain)
		//console.log("pathSuffix :"+pathSuffix)
		//console.log("url :"+url)
		//Math.floor(Math.random() * (Max - min + 1)) + min;
		//var random= Math.floor(Math.random() * (1000)) + 1;
		//console.log("random :"+random)
				
		apickli.addRequestHeader('accept',"application/json");
		apickli.addRequestHeader('Connection', "Keep-Alive");
		//console.log("\n@@@@@apickli.headers.Authorization ::::"+apickli.headers.Authorization)
		//apickli.scenarioVariables.Authorization;			
		//console.log("\n@@@@@ apickli.scenarioVariables.Authorization ::::"+apickli.scenarioVariables.Authorization)
		
		//console.log("######### pathSuffix:"+pathSuffix)
			apickli.post(pathSuffix, function (err, response) {
			if ( response ) {
				//console.log("\nRESPONSE >>>>>>:\n"+ response.body);
			}			
			apickli.storeValueOfResponseBodyPathInScenarioScope('$.reference', 'Reference');						
	        // console.log("Reference : "+apickli.scenarioVariables.Reference);			
        	callback();
		});
};





var creditByReferenceTransactionRequestWithReference = function( apickli,referenceval,getTemplateAction,callback ) {
	   
		//console.log("Reference : "+apickli.scenarioVariables.Reference);
		var pathSuffix =getTemplateAction+"/"+referenceval
		//apickli.scenarioVariables.Reference;
			
		var url = apickli.domain +pathSuffix+"/"+apickli.scenarioVariables.Reference;
		
		//console.log("pathSuffix :"+pathSuffix)
		//console.log("@url :"+url)		

		apickli.headers['Authorization'] = apickli.scenarioVariables.Authorization;	
			
		apickli.post(pathSuffix, function (err, response) {
			if ( response ) {
				//console.log("\n Cancel RESPONSE :\n"+ response.body);
			}		
			
        	callback();
		});
};

var creditByReferenceTransactionRequest = function( apickli,getTemplateAction,callback ) {
	   
		//console.log("Reference : "+apickli.scenarioVariables.Reference);
		var pathSuffix =getTemplateAction+"/"+apickli.scenarioVariables.Reference;
			
		var url = apickli.domain +pathSuffix+"/"+apickli.scenarioVariables.Reference;
		
		//console.log("pathSuffix :"+pathSuffix)
		//console.log("@url :"+url)		

		apickli.headers['Authorization'] = apickli.scenarioVariables.Authorization;	
			
		apickli.post(pathSuffix, function (err, response) {
			if ( response ) {
				//console.log("\n Cancel RESPONSE :\n"+ response.body);
			}		
			
        	callback();
		});
};

var patchVoidRequestwithReference = function( apickli,referenceval,getTemplateAction,callback ) {
	
		//console.log("Reference : "+apickli.scenarioVariables.Reference);
		var pathSuffix =getTemplateAction+"/"+referenceval
		//apickli.scenarioVariables.Reference;
					
		
		var url = apickli.domain +pathSuffix;//+"/"+apickli.scenarioVariables.Reference;			
		apickli.headers['Authorization'] = apickli.scenarioVariables.MidIdValAuthorization
		//apickli.scenarioVariables.Authorization;	
			
		apickli.patch(pathSuffix, function (err, response) {
			if ( response ) {
				//console.log("\n Cancel RESPONSE :\n"+ response.body);
			}		
			
        	callback();
		});
};



var patchVoidRequest = function( apickli,getTemplateAction,callback ) {
	
		//console.log("Reference : "+apickli.scenarioVariables.Reference);
		var pathSuffix =getTemplateAction+"/"+apickli.scenarioVariables.Reference;
					
		
		var url = apickli.domain +pathSuffix;//+"/"+apickli.scenarioVariables.Reference;			
		apickli.headers['Authorization'] = apickli.scenarioVariables.MidIdValAuthorization
		//apickli.scenarioVariables.Authorization;	
			
		apickli.patch(pathSuffix, function (err, response) {
			if ( response ) {
				//console.log("\n Cancel RESPONSE :\n"+ response.body);
			}		
			
        	callback();
		});
};



module.exports = function () { 

		this.Given(/^I have valid Templates request data$/, function (callback) 
		{		
		this.apickli.storeValueInScenarioScope("SPSUserValue", this.apickli.scenarioVariables.SPSUser);
        this.apickli.storeValueInScenarioScope("AuthorizationValue", this.apickli.scenarioVariables.Authorization);			
		callback();
		
	});
	
	this.When(/^I get data for Templates (.*)$/, function (getTemplateAction,callback) {
	      	getTemplateData(this.apickli,getTemplateAction,callback);
	});	
	
	
	
	this.When(/^I post data for transaction (.*)$/, function (getTemplateAction,callback) {		   
	      	postTransactionRequest(this.apickli,getTemplateAction,callback);
	});	
	
	this.When(/^I post data for batch (.*)$/, function (getTemplateAction,callback) {		   
	      	postTransactionBatchRequest(this.apickli,getTemplateAction,callback);
	});	
	
	this.When(/^I post data with reference as (.*) for transaction (.*)$/, function (referenceval,getTemplateAction,callback) {		   
	      	postTransactionRequestWithReference(this.apickli,referenceval,getTemplateAction,callback);
	});
		
	this.When(/^I post data for credit by reference transaction (.*)$/, function (getTemplateAction,callback) {
	      	creditByReferenceTransactionRequest(this.apickli,getTemplateAction,callback);
	});	
	
		this.When(/^I post data with reference as (.*) for credit by reference transaction (.*)$/, function (referenceval,getTemplateAction,callback) {
	      	creditByReferenceTransactionRequestWithReference(this.apickli,referenceval,getTemplateAction,callback);
	});	
	
	this.When(/^I patch data (.*)$/, function (getTemplateAction,callback) {
	      	patchVoidRequest(this.apickli,getTemplateAction,callback);
	});	
	
	this.When(/^I patch with reference as (.*) data (.*)$/, function (referenceval,getTemplateAction,callback) {
	      	patchVoidRequestwithReference(this.apickli,referenceval,getTemplateAction,callback);
	});			
	
	
	this.Given(/^I enter invalid SPS-User request data$/, function (callback) {	
		this.apickli.storeValueInScenarioScope("SPSUserValue", this.apickli.scenarioVariables.InvalidSPSUser);
        this.apickli.storeValueInScenarioScope("AuthorizationValue",this.apickli.scenarioVariables.Authorization);
	
		callback();
		
	});
	
	this.Given(/^I enter invalid Authorization request data$/, function (callback) {		
		this.apickli.storeValueInScenarioScope("SPSUserValue", this.apickli.scenarioVariables.SPSUser);
        this.apickli.storeValueInScenarioScope("AuthorizationValue", this.apickli.scenarioVariables.InvalidAuthorization);	
		callback();
		
	});
	
	
	
};

var prettyPrintJson = function(json) {
    var output = {
        stepContext: stepContext,
        testOutput: json
    };
    
    return prettyJson.render(output, {
        noColor: true
    });
};
