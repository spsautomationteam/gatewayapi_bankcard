@intg
Feature:Verify  capture action for Auth,Sale,Force and credit  type transaction

	   
	@post-transaction-patch-auth-capture   
	Scenario: Verify Transaction  'Auth' and  do a capture action as success.
	   #Given I set Authorization header to `Authorization`
	   Given I set Authorization header with MID as 999999999997 and MKEY as K3QD6YWYHFD
	   And I set content-type header to application/json
	   And I set body to {"transactionId": "5656", "retail": {   "amounts": {      "total": 19.8    },   "authorizationCode": "565656",     "orderNumber": "5659",    "cardData": {   "number": "4111111111111111",      "expiration": "0618"    },},     "transactionCode": "Authorization"}
	   When I post data for transaction /BankCard/Transactions	 			
       Then response code should be 201
       And response header Content-Type should be application/json
       And response body path $.status should be Approved       	   	 
	   And I set body to {  "transactionCode": "Capture",  "amounts": {    "total": 7.25  }} 
	   When I patch data /BankCard/Transactions
       Then response code should be 200

	   
	@post-transaction-patch-auth-capture-lower-amount   
	Scenario: Verify Transaction  'Auth' and  do a capture action as success.
	   #Given I set Authorization header to `Authorization`
	   Given I set Authorization header with MID as 999999999997 and MKEY as K3QD6YWYHFD
	   And I set content-type header to application/json
	   And I set body to {"transactionId": "5656", "retail": {   "amounts": {      "total": 75.35    },   "authorizationCode": "565656",     "orderNumber": "5659",    "cardData": {   "number": "4111111111111111",      "expiration": "0618"    },},     "transactionCode": "Authorization"}
	   When I post data for transaction /BankCard/Transactions	 			
       Then response code should be 201
       And response header Content-Type should be application/json
       And response body path $.status should be Approved       	   	 
	   And I set body to {  "transactionCode": "Capture",  "amounts": {    "total": 5.00  }} 
	   When I patch data /BankCard/Transactions
       Then response code should be 200
	
		   
	@post-transaction-patch-auth-capture-greater-amount   
	Scenario: Verify Transaction  'Auth' and  do a capture action as success.
	   #Given I set Authorization header to `Authorization`
	   Given I set Authorization header with MID as 999999999997 and MKEY as K3QD6YWYHFD
	   And I set content-type header to application/json
	   And I set body to {"transactionId": "5656", "retail": {   "amounts": {      "total": 21.56    },   "authorizationCode": "565656",     "orderNumber": "5659",    "cardData": {   "number": "4111111111111111",      "expiration": "0618"    },},     "transactionCode": "Authorization"}
	   When I post data for transaction /BankCard/Transactions	 			
       Then response code should be 201
       And response header Content-Type should be application/json
       And response body path $.status should be Approved       	   	 
	   And I set body to {  "transactionCode": "Capture",  "amounts": {    "total": 35  }} 
	   When I patch data /BankCard/Transactions
       Then response code should be 200
    
	@post-transaction-sale-capture
	Scenario: Verify Transaction  'Sale' and  do a capture .
	   #Given I set Authorization header to `Authorization`
	   Given I set Authorization header with MID as 999999999997 and MKEY as K3QD6YWYHFD
	   And I set content-type header to application/json
	   And I set body to {"transactionId": "5656", "retail": {   "amounts": {      "total": 29    },   "authorizationCode": "565656",     "orderNumber": "5659",    "cardData": {   "number": "4111111111111111",      "expiration": "0618"    },},     "transactionCode": "Sale"}
	   When I post data for transaction /BankCard/Transactions	 			
       Then response code should be 201
       And response header Content-Type should be application/json
       And response body path $.status should be Approved       	   	 
	   And I set body to {  "transactionCode": "Capture",  "amounts": {    "total": 39  }} 
	   When I patch data /BankCard/Transactions
       Then response code should be 404
	   
	@post-transaction-credit-capture
	Scenario: Verify Transaction  'Credit' and  do a capture .
	   #Given I set Authorization header to `Authorization`
	   Given I set Authorization header with MID as 999999999997 and MKEY as K3QD6YWYHFD
	   And I set content-type header to application/json
	   And I set body to {"transactionId": "5656", "retail": {   "amounts": {      "total": 45    },   "authorizationCode": "565656",     "orderNumber": "5659",    "cardData": {   "number": "4111111111111111",      "expiration": "0618"    },},     "transactionCode": "Credit"}
	   When I post data for transaction /BankCard/Transactions	 			
       Then response code should be 201
       And response header Content-Type should be application/json
       And response body path $.status should be Approved       	   	 
	   And I set body to {  "transactionCode": "Capture",  "amounts": {    "total": 45  }} 
	   When I patch data /BankCard/Transactions
       Then response code should be 404
	   
	@post-transaction-auth-capture-again-capture
	Scenario: Verify Transaction  'Credit' and  do a capture .
	   #Given I set Authorization header to `Authorization`
	   Given I set Authorization header with MID as 999999999997 and MKEY as K3QD6YWYHFD
	   And I set content-type header to application/json
	   And I set body to {"transactionId": "5656", "retail": {   "amounts": {      "total": 56    },   "authorizationCode": "565656",     "orderNumber": "5659",    "cardData": {   "number": "4111111111111111",      "expiration": "0618"    },},     "transactionCode": "Authorization"}
	   When I post data for transaction /BankCard/Transactions	 			
       Then response code should be 201
       And response header Content-Type should be application/json
       And response body path $.status should be Approved       	   	 
	   And I set body to {  "transactionCode": "Capture",  "amounts": {    "total": 56 }} 
	   When I patch data /BankCard/Transactions	   
       Then response code should be 200	   
	   And I set body to {  "transactionCode": "Capture",  "amounts": {    "total": 56  }} 
	   When I patch data /BankCard/Transactions
	   Then response code should be 404
	 	  
	@post-transaction-auth-capture-invalid-json-body   
	Scenario: Verify Transaction  'Auth' and  do a capture action as success.
	   #Given I set Authorization header to `Authorization`
	   Given I set Authorization header with MID as 999999999997 and MKEY as K3QD6YWYHFD
	   And I set content-type header to application/json
	   And I set body to {"transactionId": "5656", "retail": {   "amounts": {      "total": 220    },   "authorizationCode": "565656",     "orderNumber": "5659",    "cardData": {   "number": "4111111111111111",      "expiration": "0618"    },},     "transactionCode": "Authorization"}
	   When I post data for transaction /BankCard/Transactions	 			
       Then response code should be 201
       And response header Content-Type should be application/json
       And response body path $.status should be Approved       	   	 
	   And I set body to {  "transactionCode": "pqr",  "amounts": {    "total": 7.25  }} 
	   When I patch data /BankCard/Transactions
       Then response code should be 400	
	   And response body path $.errorCode should be InvalidRequestData  
	   And response body path $.errorDescription should be request: The TransactionCode field is required.; Error converting value  
	   


	   