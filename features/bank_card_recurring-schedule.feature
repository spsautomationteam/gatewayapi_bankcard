@intg
Feature:Verify  any transaction like Auth,Sale, Credit or Force for recurring -sceduel scenario
	    
	@post-transaction-sale-recurring-schedule
	Scenario Outline: Verify Transaction  'Force', 'Sale','Credit' and 'Authorization' and  do a void action to cancel it.		
	   Given I set Authorization header with MID as 999999999997 and MKEY as K3QD6YWYHFD
	   And I set content-type header to application/json
	   And I set body to {"transactionId": "5656", "retail":  {   "amounts":     {      "total": <amt>},    "authorizationCode": "565656",      "orderNumber": "5656",    "cardData":  {   "number": "4111111111111111",      "expiration": "0618"    },   "isRecurring": true,  "recurringSchedule": {       "amount":  294,       "frequency": "Monthly",       "interval": 1,       "nonBusinessDaysHandling": "After",       "startDate": "2017-08-30T05:05:00.546Z",       "totalCount": 2,          }    },       "transactionCode": "<txnCode>"  }      
	   When I post data for transaction /BankCard/Transactions      
       Then response header Content-Type should be application/json
       Then response body path $.status should be Approved    
       Then response code should be 201	   
	Examples:
	|txnCode		|amt|
	|Sale   		|295|
	|Credit   		|296|
	|Force   		|297|
	|Authorization  |298|
	
	
	