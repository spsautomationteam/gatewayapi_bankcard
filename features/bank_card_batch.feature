@intg
Feature:Verify  settle batch functionalities
	   
    @post-settle-batch  
	Scenario: Verify Settle batches		
		Given I set Authorization header to `Authorization`
		And I set content-type header to application/json
		And I set body to {  "settlementType": "Bankcard" }	    
		When I post data for batch /BankCard/Batches/Current				
        Then response code should be 201
        And response header Content-Type should be application/json
        And response body path $.status should be Approved	
		
    @post-settle-batch-invalid-count-net-values  
	Scenario: Verify Settle batches		
		Given I set Authorization header to `Authorization`
		And I set content-type header to application/json
		And I set body to {   "settlementType": "Bankcard",   "count": -5,   "net": -6,   "terminalNumber": "1234" }
		When I post data for batch /BankCard/Batches/Current				
        Then response code should be 200
        And response header Content-Type should be application/json        
		And response body path $.message should be BATCH INQUIRY
		
		
    @post-settle-batch-invalid-terminal-number
	Scenario: Verify Settle batches		
		Given I set Authorization header to `Authorization`
		And I set content-type header to application/json
		And I set body to {   "settlementType": "Bankcard",   "count": 0,   "net": 0,   "terminalNumber": "1" }
		When I post data for batch /BankCard/Batches/Current				
        Then response code should be 200
        And response header Content-Type should be application/json
        And response body path $.message should be BATCH INQUIRY			
		
		
    @post-settle-batch-invalid-settlement-type-ach 
	Scenario: Verify Settle batches with invalid settlement type		
		Given I set Authorization header to `Authorization`
		And I set content-type header to application/json
		And I set body to {  "settlementType": "ACH" }	    
		When I post data for transaction /BankCard/Batches/Current				
        Then response code should be 500
        And response header Content-Type should be application/json
        And response body path $.message should be An error has occurred	 
		And response body path $.exceptionMessage should be Input string was not in a correct format
		And response body path $.exceptionType should be System.FormatException
		
    @post-settle-batch-invalid-settlement-type-XYz 
	Scenario: Verify Settle batches with invalid settlement type		
		Given I set Authorization header to `Authorization`
		And I set content-type header to application/json
		And I set body to {  "settlementType": "XYz" }	    
		When I post data for transaction /BankCard/Batches/Current				
        Then response code should be 400
        And response header Content-Type should be application/json
        And response body path $.errorCode should be InvalidRequestData
		And response body path $.errorDescription should be request: You must specify either a SettlementType or one and only one of the following: Bankcard, PurchaseCard; Error converting

    @post-settle-batch-withExactNetCount  
	Scenario: Verify Settle batches		
		Given I set Authorization header to `Authorization`
		And I set content-type header to application/json
		And I set body to {  "settlementType": "Bankcard" }	    
		When I post data for batch /BankCard/Batches/Current				
        Then response code should be 200
		Given I set body to {"transactionId": "5656", "retail": {   "amounts": {      "total": 85.16    },   "authorizationCode": "565656",     "orderNumber": "5656",    "cardData": {   "number": "4111111111111111",      "expiration": "0620"  ,"cvv":"123"  },},     "transactionCode": "Sale"}
	    When I post data for transaction /BankCard/Transactions	 			
        Then response code should be 201
		Given I set body to {  "settlementType": "Bankcard","count": 1,   "net": 85.16 }	    
		When I post data for batch /BankCard/Batches/Current	
		Then response code should be 201
        And response header Content-Type should be application/json
        And response body path $.status should be Approved	
