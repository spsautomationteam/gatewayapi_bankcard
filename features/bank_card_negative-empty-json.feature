@intg
Feature:Verify transaction and settling with empty json string

	   
	@post-transaction-emptyjson-sale-end-point   
	Scenario: Verify Transaction  'Auth' and  do a capture action as success.
	   #Given I set Authorization header to `Authorization`
	   Given I set Authorization header with MID as 999999999997 and MKEY as K3QD6YWYHFD
	   And I set content-type header to application/json
	   And I set body to {}
	   When I post data for transaction /BankCard/Transactions
	   Then response code should be 400
       And response header Content-Type should be application/json
       And response body path $.errorCode should be InvalidRequestData   	   
	   And response body path $.errorDescription should be The TransactionCode field is required  
	   
	@post-settle-batch-emptyjson-end-point  
	Scenario: Verify Settle batches with empty json data		
		Given I set Authorization header to `Authorization`
		And I set content-type header to application/json
		And I set body to {}    
		When I post data for transaction /BankCard/Batches/Current				
        Then response code should be 400
        And response header Content-Type should be application/json
        And response body path $.errorCode should be InvalidRequestData   	   
	    And response body path $.errorDescription should be request: You must specify either a SettlementType or one and only one of the following: Bankcard, PurchaseCard


