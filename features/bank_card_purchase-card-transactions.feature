@intg
Feature:Verify  purchase card transaction and response
    
	@post-transaction-sale-purchase-card
	Scenario Outline: Verify Transaction  'Sale', with purchase card and verify response
	   Given I set Authorization header with MID as 999999999997 and MKEY as K3QD6YWYHFD
	   And I set content-type header to application/json
	   And I set body to {   "retail": {     "billing": {       "name": "Henry",       "address": "111 Main street",       "city": "Manassas",       "state": "VA",       "postalCode": "20112"     },     "shipping": {       "name": "Henry",       "address": "111 Main street",       "city": "Manassas",       "state": "VA",       "postalCode": "20112"     },     "level3": {       "customernumber": "123456",       "DestinationCountryCode": "USA",       "vat": {         "idnumber": "123",         "amount": 1       },       "amounts": {         "nationaltax": 1,         "discount": 0,         "duty": 0       }     },     "amounts": {       "total": <amount>,       "tax": 1     },     "cardData": {       "number": "<cardnum>",       "expiration": "0520"     }   },   "transactionCode": "<txnCode>" }
	   When I post data for transaction /BankCard/Transactions	 			
       Then response code should be 201
       And response header Content-Type should be application/json
       And response body path $.status should be Approved       	   	        
	   And response body path $.isPurchaseCard should be true	        
	Examples:
	|txnCode|cardnum|amount|
	|Sale|4128123412341231|93|	 
	|Sale|4128123412341231|93|
	#|Credit|5499740000000065|123| - not working need to enable
	 


