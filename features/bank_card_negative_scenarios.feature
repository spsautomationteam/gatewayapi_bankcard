@intg
Feature:Verify  any transaction like Auth,Sale, Credit or Force for negative scenarios

	   
	@post-sale-invalid-cardnumber
	Scenario Outline: Verify Transaction  'Force', 'Sale','Credit' and 'Authorization' with invalid cardnumber		
	   Given I set Authorization header with MID as 999999999997 and MKEY as K3QD6YWYHFD
	   And I set content-type header to application/json
	   And I set body to {"transactionId": "5656", "retail": {   "amounts": {      "total": 5656    },   "authorizationCode": "565656",     "orderNumber": "5656",    "cardData": {   "number": "XXX",      "expiration": "0618"    },},     "transactionCode": "<txnCode>"}
	   When I post data for transaction /BankCard/Transactions	 
       And response body path $.errorCode should be TransactionRejected 	   
	   And response body path $.errorDescription should be INVALID C_CARDNUMBER 	
       Then response code should be 400
       
    Examples:
	|txnCode|
	|Sale| 
	
    @post-sale-card-data-missing
	Scenario Outline: Verify Transaction  'Force', 'Sale','Credit' and 'Authorization' with card data missing
	   Given I set Authorization header with MID as 999999999997 and MKEY as K3QD6YWYHFD
	   And I set content-type header to application/json
	   And I set body to {"transactionId": "5656",  "retail": {    "amounts": {       "total": 85.15     },    "authorizationCode": "565656",     "orderNumber": "5656",   },      "transactionCode": "Sale"  }
	   When I post data for transaction /BankCard/Transactions	 
       And response body path $.errorCode should be InvalidRequestData 	   
	   And response body path $.errorDescription should be request.Retail: CardData is required
       Then response code should be 400       
    Examples:
	|txnCode|
	|Sale| 
	
	  
	  
	  
	@post-sale-invalid-retail-tag
	Scenario Outline: Verify Transaction  'Force', 'Sale','Credit' and 'Authorization' with missing retail tag
	   Given I set Authorization header with MID as 999999999997 and MKEY as K3QD6YWYHFD
	   And I set content-type header to application/json
	   And I set body to {"transactionId": "5656", "": {   "amounts": {      "total": 5656    },   "authorizationCode": "565656",     "orderNumber": "5656",    "cardData": {   "number": "4111111111111111",      "expiration": "0618"    },},     "transactionCode": "<txnCode>"}
	   When I post data for transaction /BankCard/Transactions	 
       And response body path $.errorCode should be InvalidRequestData 	   
	   And response body path $.errorDescription should be request: You must supply one and only one of the following entities: Retail or ECommerce
       Then response code should be 400       
    Examples:
	|txnCode|
	|Sale| 
	
	
	@post-sale-missing-card-number
	Scenario Outline: Verify Transaction  'Force', 'Sale','Credit' and 'Authorization'  with missing card number
	   Given I set Authorization header with MID as 999999999997 and MKEY as K3QD6YWYHFD
	   And I set content-type header to application/json
	   And I set body to {"transactionId": "5656", "retail": {   "amounts": {      "total": 5656    },   "authorizationCode": "565656",     "orderNumber": "5656",    "cardData": {       "expiration": "0618"    },},     "transactionCode": "<txnCode>"}
	   When I post data for transaction /BankCard/Transactions	 
       And response body path $.errorCode should be InvalidRequestData 	   
	   And response body path $.errorDescription should be request.Retail.CardData: The Number field is required	
       Then response code should be 400
       
    Examples:
	|txnCode|
	|Sale|   	
 
	   
	@post-sale-invalid-cvv
	Scenario Outline: Verify Transaction  'Force', 'Sale','Credit' and 'Authorization' invalid CVV		
	   Given I set Authorization header with MID as 999999999997 and MKEY as K3QD6YWYHFD
	   And I set content-type header to application/json
	   And I set body to {"transactionId": "5656", "retail": {   "amounts": {      "total": 5656    },   "authorizationCode": "565656",     "orderNumber": "5656",    "cardData": {   "number": "4111111111111111", "cvv":"xys" ,    "expiration": "0918"    },},     "transactionCode": "<txnCode>"}
	   When I post data for transaction /BankCard/Transactions	 
       And response body path $.errorCode should be TransactionRejected 	   
	   And response body path $.errorDescription should be CVV FAILURE - M 650104 	
       Then response code should be 400
       
    Examples:
	|txnCode|
	|Sale|

		   
	@post-sale-invalid-MID-MKEY
	Scenario Outline: Verify Transaction  'Force', 'Sale','Credit' with invalid MID and Invalid MKEY		
	   Given I set Authorization header with MID as xyz4 and MKEY as 123a
	   And I set content-type header to application/json
	   And I set body to {"transactionId": "5656", "retail": {   "amounts": {      "total": 5656    },   "authorizationCode": "565656",     "orderNumber": "5656",    "cardData": {   "number": "4111111111111111", "cvv":"xys",     "expiration": "0918"    },},     "transactionCode": "<txnCode>"}
	   When I post data for transaction /BankCard/Transactions	 
       And response body path $.errorCode should be InvalidCredentials 	   
	   And response body path $.errorDescription should be The client credentials supplied are invalid 	
       Then response code should be 401
       
    Examples:
	|txnCode|
	|Sale|
	
	@post-sale-invalid-authorization
	Scenario Outline: Verify Transaction  'Force', 'Sale','Credit' and 'Authorization' invalid Autorization value			 
	   Given I set Authorization header to xyz
	   And I set content-type header to application/json
	   And I set body to {"transactionId": "5656", "retail": {   "amounts": {      "total": 5656    },   "authorizationCode": "565656",     "orderNumber": "5656",    "cardData": {   "number": "4111111111111111", "cvv":"xys"  ,   "expiration": "0918"    },},     "transactionCode": "<txnCode>"}
	   When I post data for transaction /BankCard/Transactions	        	
	   And response body path $.errorCode should be InvalidHeaders 	   
	   And response body path $.errorDescription should be Required Authorization header not present 	
       Then response code should be 401
       
    Examples:
	|txnCode|
	|Sale|

	@post-sale-invalid-negative-total
	Scenario Outline: Verify Transaction  'Force', 'Sale','Credit' with invalid total amount		 
	  Given I set Authorization header with MID as 999999999997 and MKEY as K3QD6YWYHFD
	   And I set content-type header to application/json
	   And I set body to {"transactionId": "5656", "retail": {   "amounts": {      "total": -14    },   "authorizationCode": "565656",     "orderNumber": "5656",    "cardData": {   "number": "4111111111111111",    "expiration": "0918"    },},     "transactionCode": "<txnCode>"}
	   When I post data for transaction /BankCard/Transactions	 
	   And response body path $.errorCode should be InvalidRequestData 	   
	   And response body path $.errorDescription should be request.Retail.Amounts: Invalid Total Amount 	
       Then response code should be 400
       
    Examples:
	|txnCode|
	|Sale|
	
		
	@post-sale-invalid-zero-total
	Scenario Outline: Verify Transaction  'Force', 'Sale','Credit' with  total amount	 as zero	 
	  Given I set Authorization header with MID as 999999999997 and MKEY as K3QD6YWYHFD
	   And I set content-type header to application/json
	   And I set body to {"transactionId": "5656", "retail": {   "amounts": {      "total": 0    },   "authorizationCode": "565656",     "orderNumber": "5656",    "cardData": {   "number": "4111111111111111",    "expiration": "0918"    },},     "transactionCode": "<txnCode>"}
	   When I post data for transaction /BankCard/Transactions	 
	   And response body path $.errorCode should be InvalidRequestData 	   
	   And response body path $.errorDescription should be request.Retail.Amounts: Invalid Total Amount 	
       Then response code should be 400    
    Examples:
	|txnCode|
	|Sale|
	

	@post-sale-invalid-transactionCode
	Scenario Outline: Verify Transaction  'Force', 'Sale','Credit' with invalid transaction code
	  Given I set Authorization header with MID as 999999999997 and MKEY as K3QD6YWYHFD
	   And I set content-type header to application/json
	   And I set body to {"transactionId": "5656", "retail": {   "amounts": { "total":56       },   "authorizationCode": "565656",     "orderNumber": "5656",    "cardData": {   "number": "4111111111111111",    "expiration": "0918"    },},     "transactionCode": "<txnCode>"
	   When I post data for transaction /BankCard/Transactions	 
	   And response body path $.errorCode should be InvalidRequestData 	   
	   And response body path $.errorDescription should be request: This route can be used only for Authorization, Sale, Credit, or Force 	
       Then response code should be 400       
    Examples:
	|txnCode|
	|123|


	
	

	@post-sale-param-total-removed
	Scenario Outline: Verify Transaction   with total parameterre moved
	  Given I set Authorization header with MID as 999999999997 and MKEY as K3QD6YWYHFD
	   And I set content-type header to application/json
	   And I set body to {"transactionId": "5656", "retail": {   "amounts": {     },   "authorizationCode": "565656",     "orderNumber": "5656",    "cardData": {   "number": "4111111111111111",    "expiration": "0918"    },},     "transactionCode": "<txnCode>"
	   When I post data for transaction /BankCard/Transactions	 
	   And response body path $.errorCode should be InvalidRequestData 	   
	   And response body path $.errorDescription should be request.Retail: Total is required
       Then response code should be 400       
    Examples:
	|txnCode|
	|123|
	
	@post-sale-param-transactioncode-removed
	Scenario: Verify Transaction   with  transaction code removed
	  Given I set Authorization header with MID as 999999999997 and MKEY as K3QD6YWYHFD
	   And I set content-type header to application/json
	   And I set body to {"transactionId": "5656",  "retail": {      "amounts": {    "total":27       },     "authorizationCode": "565656",     "orderNumber": "5656",     "cardData": {       "number": "4111111111111111",       "expiration": "1220"     }, },  }
	   When I post data for transaction /BankCard/Transactions	 
	   And response body path $.errorCode should be InvalidRequestData 	   
	   And response body path $.errorDescription should be request: The TransactionCode field is required.
       Then response code should be 400    


	@post-sale-param-expiry-removed
	Scenario: Verify Transaction   with expiry parameter removed
	  Given I set Authorization header with MID as 999999999997 and MKEY as K3QD6YWYHFD
	   And I set content-type header to application/json
	   And I set body to {"transactionId": "5656",  "retail": {      "amounts": {    "total":27       },     "authorizationCode": "565656",     "orderNumber": "5656",     "cardData": {       "number": "4111111111111111"     }, },      "transactionCode": "Sale" }
	   When I post data for transaction /BankCard/Transactions	 
	   And response body path $.errorCode should be InvalidRequestData 	   
	   And response body path $.errorDescription should be request.Retail.CardData: The Expiration field is required.
       Then response code should be 400    

	@post-sale-param-invalid-expiry-date
	Scenario: Verify Transaction   with expiry parameter removed
	  Given I set Authorization header with MID as 999999999997 and MKEY as K3QD6YWYHFD
	   And I set content-type header to application/json
	   And I set body to {"transactionId": "5656",  "retail": {      "amounts": {    "total":27       },     "authorizationCode": "565656",     "orderNumber": "5656",     "cardData": {       "number": "4111111111111111"   ,"expiration": "12345"  }, },      "transactionCode": "Sale" }
	   When I post data for transaction /BankCard/Transactions	 
	   And response body path $.errorCode should be InvalidRequestData 	   
	   And response body path $.errorDescription should be request.Retail.CardData: The field Expiration must be a string with a minimum length of 4 and a maximum length of 4.
       Then response code should be 400   

    @post-sale-param-expiry-date-already-passed
	Scenario: Verify Transaction   with expiry parameter removed
	  Given I set Authorization header with MID as 999999999997 and MKEY as K3QD6YWYHFD
	   And I set content-type header to application/json
	   And I set body to {"transactionId": "5656",  "retail": {      "amounts": {    "total":27       },     "authorizationCode": "565656",     "orderNumber": "5656",     "cardData": {       "number": "4111111111111111"   ,"expiration": "1116"  }, },      "transactionCode": "Sale" }
	   When I post data for transaction /BankCard/Transactions	 
	   And response body path $.status should be Declined 	   
	   And response body path $.message should be CARD EXP	   
       Then response code should be 200  

 
	@post-sale-param-cardnumber-removed
	Scenario: Verify Transaction   with card number parameter removed
	  Given I set Authorization header with MID as 999999999997 and MKEY as K3QD6YWYHFD
	   And I set content-type header to application/json
	   And I set body to {"transactionId": "5656",  "retail": {      "amounts": {    "total":27       },     "authorizationCode": "565656",     "orderNumber": "5656",     "cardData": {        }, },      "transactionCode": "Sale" }
	   When I post data for transaction /BankCard/Transactions	 
	   And response body path $.errorCode should be InvalidRequestData 	   
	   And response body path $.errorDescription should be request.Retail.CardData: The Number field is required.
       Then response code should be 400    

	@post-sale-duplicate-transaction
	Scenario: Verify Transaction   with invalid transaction code
	  Given I set Authorization header with MID as 999999999997 and MKEY as K3QD6YWYHFD
	   And I set content-type header to application/json
	   And I set body to { "transactionId": "5656",  "retail": {      "amounts": {       "total": 26       },     "authorizationCode": "565656",     "orderNumber": "5656",     "cardData": {       "number": "4111111111111111",       "expiration": "1220"     }, },      "transactionCode": "Sale" }
	   When I post data for transaction /BankCard/Transactions	 
	   When I post data for transaction /BankCard/Transactions	
	   And response body path $.errorCode should be TransactionRejected 	   
	   And response body path $.errorDescription should be DUPLICATE TRANS
       Then response code should be 400    
	
	
	@post-sale-with-other-content-type
	Scenario: Verify Transaction   with content type  as application/x-www-form-urlencoded
	  Given I set Authorization header with MID as 999999999997 and MKEY as K3QD6YWYHFD
	   And I set content-type header to application/x-www-form-urlencoded
	   And I set body to { "transactionId": "5656",  "retail": {      "amounts": {       "total": 26       },     "authorizationCode": "565656",     "orderNumber": "5656",     "cardData": {       "number": "4111111111111111",       "expiration": "1220"     }, },      "transactionCode": "Sale" }
	   When I post data for transaction /BankCard/Transactions	 
	   Then response code should be 400   
	   Then response body path $.message should be No request content was found   
	    	
	
	
	@post-transaction-sale-credit-by-reference-invalid-referencenumber   
	Scenario: Verify Transaction  'Sale' and  do a 'credit By reference' action with invalid reference number
	   Given I set Authorization header to `Authorization`
	   And I set content-type header to application/json	
	   And I set body to { "transactionId": "5656", "retail": {   "amounts": {      "total":  11  },   "authorizationCode": "56556",     "orderNumber": "5656",    "cardData": {   "number": "4111111111111111",      "expiration": "0618"    },},     "transactionCode": "Sale"}
	   When I post data for transaction /BankCard/Transactions	 			
       Then response code should be 201
       And response header Content-Type should be application/json
       And response body path $.status should be Approved       	   
	   Given I set Authorization header to `Authorization`	         
	   And I set body to {"transactionCode": "CreditByReference","amount":10 }
	   #When I post data for credit by reference transaction /BankCard/Transactions
	   When I post data with reference as 123ab for credit by reference transaction /BankCard/Transactions	   
       Then response code should be 400
	   Then response body path $.errorCode should be TransactionRejected
	   Then response body path $.errorDescription should be INVALID T_REFERENCE

	
	@post-transaction-patch-auth-capture-invalid-referencenumber   
	Scenario: Verify Transaction  'Auth' and  do a capture action with invalid reference number
	   #Given I set Authorization header to `Authorization`
	   Given I set Authorization header with MID as 999999999997 and MKEY as K3QD6YWYHFD
	   And I set content-type header to application/json
	   And I set body to {"transactionId": "5656", "retail": {   "amounts": {      "total": 23.25    },   "authorizationCode": "565656",     "orderNumber": "5659",    "cardData": {   "number": "4111111111111111",      "expiration": "0618"    },},     "transactionCode": "Authorization"}
	   When I post data for transaction /BankCard/Transactions	 			
       Then response code should be 201
       And response header Content-Type should be application/json
       And response body path $.status should be Approved       	   	 
	   And I set body to {  "transactionCode": "Capture",  "amounts": {    "total": 5.00  }} 
	   #When I patch data /BankCard/Transactions
	   When I patch data with reference as 123ab for transaction /BankCard/Transactions
       Then response code should be 404
	   
	   
	Scenario Outline: Verify Transaction  'Force', 'Sale','Credit' and 'Authorization' and  do a void action  with invalid reference number		
	   Given I set Authorization header with MID as 999999999997 and MKEY as K3QD6YWYHFD
	   And I set content-type header to application/json
	   And I set body to {"transactionId": "5656", "retail": {   "amounts": {      "total": 66    },   "authorizationCode": "565656",     "orderNumber": "5656",    "cardData": {   "number": "4111111111111111",      "expiration": "0618"    },},     "transactionCode": "<txnCode>"}
	   When I post data for transaction /BankCard/Transactions	 			
       Then response code should be 201
       And response header Content-Type should be application/json
       And response body path $.status should be Approved       	   	        
	   And I set body to {  "transactionCode": "Void"}  
	   When I patch data with reference as 123ab for transaction /BankCard/Transactions
	   #I patch data /BankCard/Transactions
       Then response code should be 404
    Examples:
	|txnCode|
	|Sale|
	
	@post-sale-Wrong-card-CVV
	Scenario Outline: Verify CVV value for different cards[Amex expects 4 digit cvv ,others 3 digit]
	   Given I set Authorization header with MID as 999999999997 and MKEY as K3QD6YWYHFD
	   And I set content-type header to application/json
	   And I set body to {"transactionId": "5656", "retail": {   "amounts": {      "total": 5656    },   "authorizationCode": "565656",     "orderNumber": "5656",    "cardData": {   "number": "<cardNumber>", "cvv":"<CVVCode>" ,    "expiration": "0918"    },},     "transactionCode": "Sale"}
	   When I post data for transaction /BankCard/Transactions	 
       And response body path $.errorCode should be TransactionRejected 	   
	   #And response body path $.errorDescription should be CVV FAILURE - M 650104 	
       Then response code should be 400       
    Examples:
	|cardNumber|CVVCode|
	|4111111111111111|1234|
	|5499740000000057|1234|
	|6011000993026909|1234|
	|371449635392376|123|
	

	@post-sale-Amex-card-6digit-cvv
	Scenario Outline: Verify CVV value for amex card entering morethan 4 digit cvv[Amex expects 4 digit cvv ]
	   Given I set Authorization header with MID as 999999999997 and MKEY as K3QD6YWYHFD
	   And I set content-type header to application/json
	   And I set body to {"transactionId": "5656", "retail": {   "amounts": {      "total": 5656    },   "authorizationCode": "565656",     "orderNumber": "5656",    "cardData": {   "number": "<cardNumber>", "cvv":"<CVVCode>" ,    "expiration": "0918"    },},     "transactionCode": "Sale"}
	   When I post data for transaction /BankCard/Transactions	 
       And response body path $.errorCode should be InvalidRequestData 	   
	   And response body path $.errorDescription should be request.Retail.CardData: The field CVV must be a string or array type with a maximum length of '4'
       Then response code should be 400       
    Examples:
	|cardNumber|CVVCode|
	|371449635392376|123456|
	
	