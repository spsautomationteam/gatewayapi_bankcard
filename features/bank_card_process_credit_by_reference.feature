@intg
Feature:Verify  transaction and credit by reference.

	   
	@post-transaction-sale-credit-by-reference   
	Scenario: Verify Transaction  'Sale' and  do a 'credit By reference' action to return amount back to customer.
	   Given I set Authorization header to `Authorization`
	   And I set content-type header to application/json	
	   And I set body to { "transactionId": "5656", "retail": {   "amounts": {      "total":  111  },   "authorizationCode": "56556",     "orderNumber": "5656",    "cardData": {   "number": "4111111111111111",      "expiration": "0618"    },},     "transactionCode": "Sale"}
	   When I post data for transaction /BankCard/Transactions	 			
       Then response code should be 201
       And response header Content-Type should be application/json
       And response body path $.status should be Approved 
       And I set body to {  "settlementType": "Bankcard" }	    
	   When I post data for batch /BankCard/Batches/Current				 
	   Given I set Authorization header to `Authorization`	         
	   And I set body to {"transactionCode": "CreditByReference","amount":111 }
	   When I post data for credit by reference transaction /BankCard/Transactions	
       Then response code should be 201


	@post-transaction-sale-credit-by-reference-lower-amount   
	Scenario: Verify Transaction  'Sale' and  do a 'credit By reference' action to return a lower amount back to customer.
	   Given I set Authorization header to `Authorization`
	   And I set content-type header to application/json	
	   And I set body to { "transactionId": "5656", "retail": {   "amounts": {      "total":  15  },   "authorizationCode": "56556",     "orderNumber": "5656",    "cardData": {   "number": "4111111111111111",      "expiration": "0618"    },},     "transactionCode": "Sale"}
	   When I post data for transaction /BankCard/Transactions	 			
       Then response code should be 201
       And response header Content-Type should be application/json
       And response body path $.status should be Approved 
       And I set body to {  "settlementType": "Bankcard" }	    
	   When I post data for batch /BankCard/Batches/Current		        
	   Given I set Authorization header to `Authorization`	         
	   And I set body to {"transactionCode": "CreditByReference","amount":10 }
	   When I post data for credit by reference transaction /BankCard/Transactions	
       Then response code should be 201  
	   


	@post-transaction-sale-credit-by-reference-higher-amount   
	Scenario: Verify Transaction  'Sale' and  do a 'credit By reference' action to return a higher amount back to customer.
	   Given I set Authorization header to `Authorization`
	   And I set content-type header to application/json	
	   And I set body to { "transactionId": "5656", "retail": {   "amounts": {      "total":  20  },   "authorizationCode": "56556",     "orderNumber": "5656",    "cardData": {   "number": "4111111111111111",      "expiration": "0618"    },},     "transactionCode": "Sale"}
	   When I post data for transaction /BankCard/Transactions	 			
       Then response code should be 201
       And response header Content-Type should be application/json
       And response body path $.status should be Approved  
	   And I set body to {  "settlementType": "Bankcard" }	    
	   When I post data for batch /BankCard/Batches/Current				
       Given I set Authorization header to `Authorization`	         
	   And I set body to {"transactionCode": "CreditByReference","amount":30 }
	   When I post data for credit by reference transaction /BankCard/Transactions	
       Then response code should be 201  
	   
	@post-transaction-sale-credit-by-reference-invalid-json   
	Scenario: Verify Transaction  'Sale' and  do a 'credit By reference' action to return amount back to customer.
	   Given I set Authorization header to `Authorization`
	   And I set content-type header to application/json	
	   And I set body to { "transactionId": "5656", "retail": {   "amounts": {      "total":  118  },   "authorizationCode": "56556",     "orderNumber": "5656",    "cardData": {   "number": "4111111111111111",      "expiration": "0618"    },},     "transactionCode": "Sale"}
	   When I post data for transaction /BankCard/Transactions	 			
       Then response code should be 201
       And response header Content-Type should be application/json
       And response body path $.status should be Approved
       And I set body to {  "settlementType": "Bankcard" }	    
	   When I post data for batch /BankCard/Batches/Current 
	   Given I set Authorization header to `Authorization`	         
	   And I set body to {"transactionCode": "abc","amount":100 }
	   When I post data for credit by reference transaction /BankCard/Transactions	
       Then response code should be 400
	   And response body path $.errorCode should be InvalidRequestData   
	   And response body path $.errorDescription should be request: The TransactionCode field is required.; Error converting value
	   
	    





